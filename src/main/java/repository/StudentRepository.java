package repository;

import model.Student;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {
    @Query(value = "select * from studentTable order by groupId", nativeQuery = true)
    List<Student> orderAllByGroupId();
    @Query(value = "select * from studentTable where groupId = ?", nativeQuery = true)
    List<Student> findAllByGroupId(long groupId);

}