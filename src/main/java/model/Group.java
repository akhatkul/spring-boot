package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "groupTable")
@Data
@AllArgsConstructor
@NoArgsConstructor

public class Group {
    @Id
    private long groupId;
    @Column
    private String name;
}
